#include "../libs/Display.h"
#include <boost/algorithm/string.hpp>

Display::Display() {
	std::string temp_str = SystemProfiler::get_data("SPHardwareDataType");
	hardware = SPHardwareDataType(temp_str);

    if (boost::algorithm::iequals("macbook8,1",hardware.get_modelidentifier().first)) {
        temp_str = SystemProfiler::get_data("SPNVMeDataType");
    } else {
        if (!hardware.is_SATA())
            temp_str = SystemProfiler::get_data("SPParallelATADataType");
        else
            temp_str = SystemProfiler::get_data("SPSerialATADataType");
    }
	sata = SPSATADataType(temp_str);

	temp_str = SystemProfiler::get_data("SPAirPortDataType");
	airport = SPAirPortDataType(temp_str);

	temp_str = SystemProfiler::get_data("SPBluetoothDataType");
	bluetooth = SPBluetoothDataType(temp_str);

	if (hardware.has_diskdrive()) {
		temp_str = SystemProfiler::get_data("SPDiscBurningDataType");
		disc = SPDiscBurningDataType(temp_str);
	}

	temp_str = SystemProfiler::get_data("SPDisplaysDataType");
	display = SPDisplaysDataType(temp_str);

	temp_str = SystemProfiler::get_data("SPMemoryDataType");
	memory = SPMemoryDataType(temp_str);

	if (hardware.has_battery()) {
		temp_str = SystemProfiler::get_data("SPPowerDataType");
		power = SPPowerDataType(temp_str);
	}
}

void Display::run() {
	_check_success("SPHardwareDataType", hardware);
	_check_success("SPSATADataType", sata);
	_check_success("SPMemoryDataType", memory);
	_check_success("SPDisplaysDataType", display);
	_check_success("SPDiscBurningDataType", disc);
	_check_success("SPAirPortDataType", airport);
	_check_success("SPBluetoothDataType", bluetooth);
	_check_success("SPPowerDataType", power);
	//std::cout << "Number to fix: " << this->to_fix.size() << std::endl;
	std::cout << std::endl << "----- Press ENTER to continue -----" << std::endl;
	std::cin.ignore(std::numeric_limits<std::streamsize> ::max(), '\n');

	if (this->to_fix.size() != 0) {
		fix();
	}
	//submit();
}

void Display::fix() {
	if (this->to_fix.empty())
		return;
	for (std::vector<SPDataType*>::iterator it = this->to_fix.begin();
			it != this->to_fix.end(); it++) {
		(**it).set_all();
	}
	this->to_fix.clear();
}

bool Display::_check_success(std::string input, SPDataType& curr_obj) {
	if ((boost::algorithm::iequals(input, "SPPowerDataType") && !this->hardware.has_battery()) ||
			(input.find("Burning") != std::string::npos && !hardware.has_diskdrive())) {
		std::cout << input << " Check:			N/A" << std::endl;
		return true;
	}
	if (!curr_obj.success()) {
		std::cout << input << " Check:   		Failed!" << std::endl;
		this->to_fix.push_back(&curr_obj);
		return false;
	} else
		std::cout << input << " Check:   		Passed!" << std::endl;
	return true;
}
std::string Display::_get_JSon() {
	std::stringstream ss;
	
	ss<<"{";
    if (hardware.has_diskdrive())
        ss<<"\"DiscBurning\":"<<disc.output()<<",";
	ss<<"\"Graphics/Displays\":" <<display.output()<<",";
	ss<<"\"Power\":"<<power.output()<<",";
	ss<<"\"Hardware\":"<<hardware.output()<<",";
	ss<<"\"Serial-ATA\":"<<sata.output()<<",";
	ss<<"\"Memory\":"<<memory.output();
	
	ss<<"}";

	
	return ss.str();

}
std::string Display::_get_Username() {


	return "test";

}
std::string Display::_get_ID() {


	return hardware.get_serialnumber().first;

}
