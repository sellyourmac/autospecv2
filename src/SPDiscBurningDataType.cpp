#include "../libs/SPDiscBurningDataType.h"

SPDiscBurningDataType::SPDiscBurningDataType(std::string raw_input) : SPDataType(raw_input, "SPDiscBurningDataType") {
	YAML::Node& node = _data;
	dvd_read = _get_dvd_read(node);
	cd_write = _get_cd_write(node);
	
	
	//Placeholder setting for model number
	model.first = "Model Placeholder";
	model.second = true;
	
	while (node.IsMap()) {
		if (_string_convert(node["Cache"]).second)
			break;
		node = node.begin()->second;
	}

	dvd_write = _string_convert(node["DVD-Write"]);
}

std::pair<bool, bool> SPDiscBurningDataType::_get_dvd_read(YAML::Node node) const {
	while (node.IsMap()) {
		if (_string_convert(node["Cache"]).second)
			break;
		node = node.begin()->second;
	}
	std::pair<std::string, bool> ret_str = _string_convert(node["Reads DVD"]);
	if (ret_str.second) {
		if (boost::algorithm::iequals(ret_str.first, "Yes")) 
			return std::pair<bool, bool>(true,true);
		else
			return std::pair<bool, bool>(false,true);
	}
	return std::pair<bool, bool>(false, false);
}

std::pair<std::string, bool> SPDiscBurningDataType::_get_cd_write(YAML::Node node) const {
	while (node.IsMap()) {
		if (_string_convert(node["Cache"]).second)
			break;
		node = node.begin()->second;
	}
	return _string_convert(node["CD-Write"]);
}

bool SPDiscBurningDataType::success() const {
	if (!dvd_read.second || !cd_write.second)
		return false;
	else 
		return true;
}

std::string SPDiscBurningDataType::output() const {
	std::stringstream ss;
	
	ss<<"{";
		ss<<"\"ReadsDualLayer\":";
			if(dvd_write.first.find("DL") !=  std::string::npos)
				ss<<"true,";
			else
				ss<<"false,";
		ss<<"\""<<model.first; ss<<"\":";
			ss<<"{";
			ss<<"\"CD-Write\":"<< "\""<<cd_write.first;
			ss<<"\",";
			ss<<"\"ReadsDVD\":";
			if(dvd_read.first)
				ss<<"true";
			else
				ss<<"false";
			ss<<",";
			ss<<"\"DVD-Write\":"<<"\""<<dvd_write.first;
			ss<<"\"";
			ss<<"}";
	ss<<"}";
	
	
	
	return ss.str();
}
