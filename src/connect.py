import urllib2
import sys
import re
from time import sleep

import pickle
import json
import sqlite3

inv_check = True

try:
    from inventory import InventorySite
except ImportError:
    inv_check = False
try:
    data_file = open('./src/models', 'rb')
    data = pickle.load(data_file)
except IOError:
    print "Model data not available."
    data = {}

model_db = './res/clean.db'

class Submission(object):
    def __init__(self, json, serial, username):
        #self.needID = True if args.needItem else false

        self.url = "http://inv.sellyourmac.com/util/importMacSpecs.php"
        self.backup_url = "http://54.69.86.169/submit-info.php"

        self.user = raw_input("Enter your inventory username: ")
        #print "Fix the user when we reach implementation"
        
        self.json = json
        self.serial = serial

        #self.submit(json, serial, self.user)

    def submit(self):
        data = "json="+self.json+"&id="+self.serial+"&username="+self.user
        data = self.process_data(data)
        req = urllib2.Request(self.url, data)
        for i in xrange(5):
            try:
                resp = urllib2.urlopen(req,timeout=2)
                returned = resp.read()
                break
            except Exception,e:
                print "Retrying..."
                returned = None
                sleep(1)
        code = returned if returned is not None else "Error: No server response."
        invID = re.compile(r'[a-zA-z]{2}\d{6}')
        print code
        if "itemid" in code.lower():
            self.serial = self.ask_itemid()
            code = self.submit()
            return code
        try:
            with open('/Volumes/Macintosh HD/Users/macuser/Desktop/ItemID', 'w') as f:
                f.write(code)
                f.flush()
        except IOError:
            pass
        
        if invID.search(code) is not None:
            try:
                self.backup(code)
            except Exception:
                print "Failed to backup."
            return code
        return None
            
    def backup(self,itemid):
        try:
            data = "itemid=%s&data=%s" % (itemid,self.json)
            req = urllib2.Request(self.backup_url,data)
            resp = urllib2.urlopen(req,timeout=5)
            if "success" in resp.read().lower():
                print "Successfully backed up."
        except Exception:
            print "Failed to backup."

    def ask_itemid(self):
        x = raw_input("If the Inventory ID is required please enter it now, else leave blank:\n> ")
        return x

    def process_data(self, data):
       # data['username'] = self.user
        #ret = urllib.urlencode(data)
        ret = data.replace("{", "%7B")
        ret = ret.replace("}", "%7D")
        ret = ret.replace(",", "%2C")
        ret = ret.replace('"', "%22")
        ret = ret.replace("%27", '%22')
        ret = ret.replace(u'%22', '%22')
        return ret
    
class ModelInfo:
    def __init__(self, data):
        self.data = data
        self.datum = json.loads(self.data)
        self.serial = self.datum["Hardware"]["Hardware Overview"]["Serial Number (system)"]
        self.modelident = self.datum["Hardware"]["Hardware Overview"]["Model Identifier"]
        self.clockspeed = float(self.datum["Hardware"]["Hardware Overview"]["Processor Speed"].split()[0])
        self.proc_type = self.datum["Hardware"]["Hardware Overview"]["Processor Name"]
        self.num_proc = int(self.datum["Hardware"]["Hardware Overview"]["Number of Processors"])
        self.num_cores = int(self.datum["Hardware"]["Hardware Overview"]["Total Number of Cores"])
        self.storage = self._set_storage() #list of capacities
        self.ram = self._set_ram() #stored in MB
        self.hd_speed = ""
        try:
            node = self.datum["Serial-ATA"]
            for hdd,v in node.items():
                if "ssd" in v["rate"].lower() or "solid" in v["rate"].lower():
                    self.hd_speed = "SSD"
        except Exception:
            self.hd_speed = ""
        
    def lookup(self, ram=None, vram=None, storage=None):
        conn = sqlite3.connect(model_db)
        c = conn.cursor()
        query = "SELECT computer.model_no, computer.year, computer.ram, computer.vram, computer.storage, computer.screen from computer WHERE (computer.identifier=? and computer.clock=?"
        arg_list = [self.modelident,self.clockspeed]
        if ram:
            query+=" AND computer.ram=?"
            arg_list.append(ram)
        if vram:
            query+=" AND computer.vram=?"
            arg_list.append(ram)
        if storage:
            query+=" AND computer.storage=?"
            arg_list.append(storage)
        query+=") ORDER BY computer.model_no ASC"
        
        possible = c.execute(query,arg_list).fetchall()

        print "Possible Model Numbers:"
        for possibility in possible:
            ModelInfo.pprint(possibility)
        
        if len(possible) == 1:
            return possible[0]
        return
    
    @staticmethod
    def pprint(possibility):
        if int(possibility[5]) != 0: #if screen size matters
            print "\t%s | %s | RAM: %sMB | VRAM: %sMB | HDD: %sGB | SCR: %s" % possibility
        else:
            print "\t%s | %s | RAM: %sMB | VRAM: %sMB | HDD: %sGB" % possibility[:5]
    
    def get_model_by_serial(self,serial):
        if len(serial) == 12:
            serial = serial[-4:]
        elif len(serial) == 11:
            serial = serial[-3:]
        if self.data["mbs"].get(serial) is not None:
            print "Model: %s" % self.data["mbs"].get(serial)

    def get_modelident(self):
        return self.modelident

    def get_clockspeed(self):
        return self.clockspeed
        
    def get_proc_type(self):
        return self.proc_type
    
    def get_num_proc(self):
        return self.num_proc
    
    def get_num_cores(self):
        return self.num_cores
    
    def get_storage(self):
        return self.storage
    
    def get_ram(self):
        return self.ram
    
    def get_hdspeed(self):
        return self.hd_speed
        
    def get_serial(self):
        return self.serial
        
    def _set_storage(self):
        hdd_list = self.datum["Serial-ATA"]
        storage_list = []
        for hdd,data in hdd_list.items():
            if 'tb' in data["capacity"].lower():
                storage_list.append(int(float(data["capacity"].split()[0])*1024))
            else:
                storage_list.append(int(data["capacity"].split()[0]))
        return storage_list

    def _set_ram(self):
        ram_list = self.datum["Memory"]["Memory Slots"]["ECC"]
        ret = 0
        for k,v in ram_list.items():
            try:
                size = float(v["Size"].split()[0])
                if size < 32:
                    size *= 1024
            except Exception:
                size = 0
            ret += size
        return ret
            
class QuoteCheck:
    def __init__(self, itemid, serial, mInfo,inv):
        self.valid = True
        self.m = mInfo
        self.inv = inv
        self.cosmetic_list = { "like new":"AB",
                               "good":"ABC",
                               "fair":"ABC",
                               "poor":"ABCD" }
        self.itemid = itemid
        self.serial = serial
        self.clock = mInfo.get_clockspeed()
        self.proc_type = mInfo.get_proc_type()
        self.num_cores = mInfo.get_num_cores()
        self.hdd_size = mInfo.get_storage()[0] if len(mInfo.get_storage()) < 2 else None
        self.ram = mInfo.get_ram()
        self.page = inv.get_pagebyitemid(self.itemid)
        self.cosmetic = inv.get_cosmetic(self.page)
        self.hd_speed = mInfo.get_hdspeed()
        
        self.notes = inv.get_notes(self.page)
        
        try:
            self.cust_serial, self.cust_clock, self.cust_proc_type, self.cust_hdd, self.cust_hdspeed, self.cust_ram, self.cust_cosm = self._set_cust_data()
        except ValueError:
            self.valid = False
    
    def _set_cust_data(self):
        ret = ()
        if "How is the cosmetic condition?" not in self.notes:
            raise ValueError("No quote information found.")
        splitted = self.notes.replace("\r","").split("\n")
        if len(splitted) < 11:
            raise ValueError("No quote information found.")
        start = 0
        '''for i in xrange(len(splitted)):
            if "How is the cosmetic condition".lower() in splitted[i].lower():
                start = i
                break
        data = splitted[start-10:start+1]
        ret = ret + ((data[1][8:]),) #serial
        try:
            ret = ret + ((float(data[2].split()[0])),) #clock
        except Exception:
            ret = ret + ((0),)
        start = data[2].lower().find('hz')
        end = data[2].find('(')
        ret = ret + ((data[2][start+3:end-1]),) #proc_type
        ret = ret + ((data[3].split()[0]),) #hdd
        ret = ret + (("SSD" if 'ssd' in data[3].lower() else ""),)
        ram = float(data[4].split()[0])
        ret = ret + ((self._fix_ram_mult(ram)),) #ram
        ret = ret + ("".join(data[10].split('?')[1:])[1:],) #cosmetic vals'''
        
        for i in xrange(len(splitted)):
            if 'serial:' in splitted[i].lower():
                start = i-1
                break
        data = splitted[start:]
        ret = ret + ((data[1][8:]),) #serial
        try:
            ret = ret + ((float(data[2].split()[0])),) #clock
        except Exception:
            ret = ret + ((0),)
        start = data[2].lower().find('hz')
        end = data[2].find('(')
        ret = ret + ((data[2][start+3:end-1]),) #proc_type
        #ret = ret + ((data[3].split()[0]),) #hdd
        hdd_info = data[3].split()
        try:
            if hdd_info[1].lower() == 'tb':
                ret = ret + (int(float(hdd_info[0])*1024),)
            else:
                ret = ret + ((hdd_info[0]),)
        except Exception:
            ret = ret + ((hdd_info[0]),)
        ret = ret + (("SSD" if 'ssd' in data[3].lower() else ""),)
        ram = float(data[4].split()[0])
        ret = ret + ((self._fix_ram_mult(ram)),) #ram
        cosm = [x for x in data if 'cosmetic condition' in x]
        ret = ret + ("".join(cosm[0].split('?')[1:])[1:],) #cosmetic vals
        return ret
    
    def _fix_ram_mult(self,curr_ram):
        if curr_ram < 32:
            return curr_ram * 1024
        return curr_ram
    
    def modify_oInfo(self, info):
        if info != "":
            self.inv.prepend_oInfo(self.itemid,info)
        
    def cross_check(self):
        if not self.valid:
            print "No data to compare to!"
            return
        ret = "NR: "
        if self.cust_serial.lower() != self.serial.lower():
            if self.cust_serial.replace(" ","") != "":
                ret = "Serial Numbers do not match, please confirm that this is the correct computer.\n\n" + ret
        if self.cust_clock != 0:
            if float(self.cust_clock) != float(self.clock):
                ret += 'Incorrect processor speed (Quoted: %s; Actual: %s), ' % (self.cust_clock, self.clock)
        if self.cust_proc_type.lower() not in self.proc_type.lower():
            inc = False
            if bool('i3' in self.cust_proc_type.lower()) ^ bool('i3' in self.proc_type.lower()):
                inc = True
            elif bool('i5' in self.cust_proc_type.lower()) ^ bool('i5' in self.proc_type.lower()):
                inc = True
            elif bool('i7' in self.cust_proc_type.lower()) ^ bool('i7' in self.proc_type.lower()):
                inc = True
            elif bool('quad' in self.cust_proc_type.lower()) ^ bool(self.num_cores == 4):
                inc = True
            elif bool('8-core' in self.cust_proc_type.lower()) ^ bool(self.num_cores == 8):
                inc = True
            elif bool('dual' in self.cust_proc_type.lower()) ^ bool(self.num_cores == 2):
                inc = True
            if inc:
                ret += 'Incorrect processor name (Quoted: %s; Actual: %s), ' % (self.cust_proc_type, self.proc_type)
        if int(self.hdd_size) != 0:
            if int(self.cust_hdd) != int(self.hdd_size):
                ret += 'Incorrect hdd size (Quoted: %s; Actual: %s), ' % (self.cust_hdd, self.hdd_size)
        if 'ssd' in self.cust_hdspeed.lower():
            if 'ssd' not in self.hd_speed.lower() and 'solid' not in self.hd_speed.lower():
                ret += 'Incorrect HDD Speed (Quoted: SSD; Actual: Not SSD), '
        if self.ram != self.cust_ram:
            ret += 'Incorrect ram size (Quoted: %.2f; Actual: %.2f), ' % (float(self.cust_ram)/1024, float(self.ram)/1024)
        if self.cust_cosm.lower() == "good":
            if self.cosmetic == "C":
                ret += 'Potential cosmetic problem (Quoted: %s; Actual: %s), ' % (self.cust_cosm, self.cosmetic)
            elif self.cosmetic == "D":
                ret += 'Cosmetic needs revision (Quoted: %s; Actual: %s), ' % (self.cust_cosm, self.cosmetic)
        elif self.cosmetic not in self.cosmetic_list[self.cust_cosm.lower()]:
            ret += 'Cosmetic needs revision (Quoted: %s; Actual: %s), ' % (self.cust_cosm, self.cosmetic)
        if ret[-2:] == ": ":
            ret = ""
        elif ret[-2:] == ", ":
            ret = ret[:-2]
            ret += " (AutoSpec)"
        self.modify_oInfo(ret)
        return ret
        
def output(text, spacing=0):
    ret = ""
    for key in text.iterkeys():
        if type(text[key]) == dict:
            ret += '  ' * spacing + key + ':\n'
            ret += output(text[key],spacing + 1)
        else:
            ret += '  ' * spacing + str(key) + ': ' + str(text[key]) + '\n'
    return ret

if __name__ == '__main__':
    #TODO argc argv stuff
    #modelno = get_model_by_serial(sys.argv[2])
    m = ModelInfo(sys.argv[1])
    print output(m.datum)
    model_num = m.lookup()
    s = Submission(sys.argv[1], sys.argv[2], sys.argv[3])
    code = s.submit()
    # 1 is json 2 is serial number 3 is username
    if code is not None:
        inv = InventorySite(s.user)
        if model_num:
            model_check = raw_input("Submit model number? [y/n]: ")
            if model_check.lower() == "y":
                inv.login()
                inv.set_model_number(code,str(model_num[0].encode('ascii','ignore')))
        if inv_check:
            user_check = raw_input("Type 'yes' if you want to cross check with the quote: ")
            if user_check.lower() != "yes":
                sys.exit(0)
            if not inv.logged_in:
                inv.login()
            page = inv.get_pagebyitemid(code)
            q = QuoteCheck(code,sys.argv[2],m,inv)#,m.get_clockspeed(),m.get_proc_type(), m.get_storage(), m.get_ram(), inv.get_cosmetic(page)inv.get_notes(page))
            
            cross_info = q.cross_check()
            if cross_info:
                print
                if cross_info != "":
                    print cross_info
                else:
                    print "No discrepencies found."
                print
        
