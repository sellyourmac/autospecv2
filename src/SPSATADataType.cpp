#include "../libs/SPSATADataType.h"
#include <yaml-cpp/yaml.h>
#include <iostream>
#include <stdexcept>

SPSATADataType::SPSATADataType() : SPDataType() {
	vendor = str_bool;
	product = str_bool;
	connectionType = bool_bool;
}

SPSATADataType::SPSATADataType(std::string raw_data) : SPDataType(raw_data, "SPSATADataType") {
	YAML::Node temp = _data;
    YAML::Node last = temp;
    
    if (temp["NVMExpress"]) {
        last = last["NVMExpress"];
    } else {

        while (temp.IsMap()) {
            //if (temp.size() > 1)
            if (temp["Vendor"]|| temp["ATA Bus 0"])
                break;
            last = temp;
            temp = temp.begin()->second;
        }

        vendor = _string_convert(temp["Vendor"]);
        product = _string_convert(temp["Product"]);
        connectionType = _connection_type(_string_convert(temp["Physical Interconnect"]));

        /*if (temp["FUCKING A"])
            temp = temp["FUCKING A"]; //at the hdd level now
        else
            temp = temp["ATA Bus 0"];
        */
    }
	for (YAML::iterator hdd = last.begin(); hdd != last.end(); hdd++) {
        YAML::Node temp_hdd;
        if (hdd->second["FUCKING A"]) 
            temp_hdd = hdd->second["FUCKING A"].begin()->second;
        else
            temp_hdd = hdd->second.begin()->second;
        
        if (temp_hdd.IsScalar() || !temp_hdd["Capacity"])
            continue;
		_HDDList.push_back(temp_hdd.as<HardDrive>());
	}
    
    
}

std::pair<bool, bool> SPSATADataType::_connection_type(std::pair<std::string, bool> input) {
	if (!input.second) {
		return std::pair<bool, bool>(false, false);
	}
	if (boost::algorithm::iequals(input.first, "SATA")) {
		return std::pair<bool, bool>(true, true);
	}
	return std::pair<bool, bool>(false, true);
}

std::pair<bool, bool> SPSATADataType::is_SSD(unsigned drive_num) const {
	if (drive_num < _HDDList.size()) {
		HardDrive curr_hdd = _HDDList.at(drive_num);
		return curr_hdd.MediumType;
	}
	throw(std::out_of_range("Drive not in range!"));
}

std::pair<bool, bool> SPSATADataType::is_SSD(const HardDrive& drive) const {
    return drive.MediumType;
}

unsigned SPSATADataType::get_capacity(const HardDrive& drive) const {
	int ret;
	if (drive.Capacity.second) {
		//TODO MAKE MAGIK
        if (this->is_SSD(drive).first == SSD) {
            for (unsigned num = 0; num < sizeof(SSD_SIZE_LIST) / sizeof(unsigned); num++) {
                ret = (abs(drive.Capacity.first - SSD_SIZE_LIST[num]) < abs(drive.Capacity.first - ret)) ?
                    SSD_SIZE_LIST[num] : ret;
            }
        } else {
            for (unsigned num = 0; num < sizeof(MECH_SIZE_LIST) / sizeof(unsigned); num++) {
                ret = (abs(drive.Capacity.first - MECH_SIZE_LIST[num]) < abs(drive.Capacity.first - ret)) ?
                    MECH_SIZE_LIST[num] : ret;
            }
        }
        return ret;
	}
    //std::cout << drive.Capacity.first << "\n";
	//throw(std::logic_error("Drive capacity not readable!"));
}

unsigned SPSATADataType::get_capacity(std::size_t drive_num) const {
	if (drive_num < _HDDList.size()) {
		const HardDrive& drive = _HDDList.at(drive_num);
		return get_capacity(drive);
	}
	//throw(std::out_of_range("Drive not in range!"));
}


unsigned SPSATADataType::get_total_capacity() const {
	unsigned sum = 0;
	for (std::vector<HardDrive>::const_iterator drive = _HDDList.begin(); drive != _HDDList.end(); drive++) {
		sum += get_capacity(*drive);
	}
	return sum;
}

bool SPSATADataType::success() const {
	if (!vendor.second || !product.second ||
		       _HDDList.size() == 0)
		return false;
	return true;
}

void SPSATADataType::set_all() {
	if (!vendor.second)
		set_vendor(_question("Vendor", vendor.first));
	if (!product.second)
		set_product(_question("Product", product.first));
	if (!connectionType.second) {
		std::string temp;
		temp = _question("Connection Type (SATA/ATA)", connectionType.first);
		if (boost::algorithm::iequals(temp, "SATA"))
			set_connectiontype(SATA);
		else
			set_connectiontype(!SATA);
	}
	if (_HDDList.size() == 0)
		add_harddrive();
}

void SPSATADataType::add_harddrive() {
	HardDrive hd;
	std::string res;
	std::cout << "There are no harddrives available, so I don't know how" <<
		" this is actually working. But ok. We shall push onward" <<
		std::endl << "Enter Capacity in GB: ";
	std::getline(std::cin, res);
	hd.Capacity = _strip_string(std::pair<std::string, bool>(res, true));
	std::cin.rdbuf();
	std::cin.clear();

	std::cout << "Enter Rotational Rate (e.g. SSD or 5400 or 7200): ";
	std::getline(std::cin, res);
	std::cin.rdbuf();
	std::cin.clear();
	if (boost::algorithm::iequals(res, "SSD"))
		hd.MediumType = std::pair<bool, bool>(SSD, true);
	else
		;
	
	std::cout << "Are there more drives? [y/n]: ";
	std::getline(std::cin, res);
	std::cin.rdbuf();
	std::cin.clear();
	if (boost::algorithm::iequals(res, "y"))
		add_harddrive();
}

std::string SPSATADataType::output() const {
std::stringstream ss;
	
	ss<<"{";
		for (std::vector<HardDrive>::const_iterator it = _HDDList.begin(); it != _HDDList.end(); it++) {
			ss<<"\"";
			ss<<(it->Model.first)<<"\":{";
                if (this->get_capacity(*it) >= 1024) {
                    ss<<"\"capacity\": \""<<(double)this->get_capacity(*it)/1024<<" TB\",";
                } else {
                    ss<<"\"capacity\": \""<<(this->get_capacity(*it))<<" GB\",";
                }
				if(it->MediumType.first)
				{
					ss<<"\"rate\": \""<<"SSD"<<"\",";
					ss<<"\"mediumtype\": \""<<"Solid State"<<"\"";
				}
				else
				{
					if (it->Rate.first == 0)
						ss<<"\"rate\": \""<< 5400 <<"\",";
					else
						ss<<"\"rate\": \""<< it->Rate.first <<"\",";
				ss<<"\"mediumtype\": \""<<"Rotational"<<"\"";
				}
			ss<<"}";
			if(it != _HDDList.end()-1)
				ss<<",";
		}
		
	ss<<"}";
	return ss.str();



}


