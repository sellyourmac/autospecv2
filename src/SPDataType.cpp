

#include "../libs/SPDataType.h"
#include <boost/algorithm/string.hpp>
#include <vector>
#include <map>
#include <iostream>
#include <sstream>

SPDataType::SPDataType(std::string raw_data, std::string type) {
	str_bool.first = "";
	str_bool.second = false;
	u_bool.first = 0;
	u_bool.second = false;
	bool_bool.first = false;
	bool_bool.second = false;

	_type = type;
	_clean_data = _sanitize(raw_data);
	if (_clean_data.size() != 0)
		_data = YAML::Load(_clean_data);
}

std::string SPDataType::_sanitize(std::string& raw_data) {
	bool sata = (boost::algorithm::iequals(this->_type, "spserialatadatatype") ||
			boost::algorithm::iequals(this->_type, "spparallelatadatatype") ||
			boost::algorithm::iequals(this->_type, "spsatadatatype"));
	std::pair<bool, unsigned> in_volumes(false,0);
	int drive_num = 0;
	std::map<int, int> line_spacing;
	line_spacing[0] = 0;
	std::stringstream ret;
	std::vector<std::string> raw_data_arr;

	if (!boost::algorithm::iequals(_type, "SPDiscBurningDataType"))
		boost::algorithm::replace_all(raw_data, ": -", ": Not Available");
	boost::algorithm::split(raw_data_arr, raw_data, boost::is_any_of("\n"));
	raw_data_arr = _remove_blank_lines(raw_data_arr);
	std::vector<std::string>::iterator line = raw_data_arr.begin();
	std::vector<std::string>::iterator end = raw_data_arr.end();
	for (; line != end; line++) {
		unsigned curr_space_count = _whitespace_count(*line);
		if (!in_volumes.first && boost::algorithm::iequals(_key(*line), "Volumes")) {
			in_volumes.first = true;
			in_volumes.second = curr_space_count;
			continue;
		} else if (in_volumes.first && curr_space_count <= in_volumes.second) {
			in_volumes.first = false;
		} else if (in_volumes.first) {
			continue;
		}
		if (line != raw_data_arr.begin()) {
			std::string prev_value = _value(*(line-1));
			std::string curr_value = _value(*line);
			if (sata && line_spacing.at(curr_space_count) == 2) {
				ret << std::string(line_spacing.at(curr_space_count), ' ') << 
						_remove_whitespace(*line).substr(0, 
							_remove_whitespace(*line).length()-1) <<
						" " << drive_num << ":" << std::endl;
				drive_num++;
			} else {
				if (line_spacing.at(curr_space_count) != 0) {
					ret << std::string(line_spacing.at(curr_space_count), ' ') <<
						_remove_whitespace(*line) << std::endl;
				}
			}
		}
		if (line != (end-1)) {
			if (curr_space_count == 0)
				ret << "---\n" << _remove_whitespace(*line) << std::endl;

			unsigned next_space_count = _whitespace_count(*(line+1));
			if (next_space_count > curr_space_count) {   //Check for bad indentation
				if (!boost::algorithm::iequals(_value(*line), "")) { 
					ret << std::string(line_spacing.at(curr_space_count), ' ') << "FUCKING A:" <<
						std::endl;
				}
			} 
			//line_spacing.emplace(next_space_count,
			//		line_spacing.at(curr_space_count)+2);
			if (next_space_count > curr_space_count)
				line_spacing[next_space_count] = line_spacing.at(curr_space_count)+2;
		}
	}
	//std::cout << ret.str() << std::endl;
	return ret.str();
}

void SPDataType::set_all() {
	_question("Type");
	_question("Key List");
}

std::string SPDataType::_question(std::string name) {
	std::string ret;
	std::cout << "Please enter value for '" << name << "': ";
	std::getline(std::cin, ret);
	std::string confirm;
	std::cout << "Is '" << ret << "' correct? [y/n]" << std::endl;
	std::getline(std::cin, confirm);
	if (boost::algorithm::iequals(confirm, "n"))
		return _question(name);
	return ret;
}

std::string SPDataType::_value(const std::string line) {
	std::size_t colon = line.find(":");
	if (colon+2 > line.size()-1)
		return "";
	return line.substr(colon+2);
}

std::string SPDataType::_key(const std::string line) {
	std::string temp = line;
	temp = _remove_whitespace(line);
	std::size_t colon = temp.find(":");
	return temp.substr(0,colon);
}

int SPDataType::_whitespace_count(const std::string line) {
	std::size_t len = boost::algorithm::trim_right_copy(line).length();
	std::string trimmed_line = boost::algorithm::trim_copy(line);
	std::size_t trimmed_len = trimmed_line.length();
	return (len - trimmed_len);
}

void SPDataType::_remove_blank_lines(std::string& raw_data) {
	std::vector<std::string> split_data;
	std::vector<std::string> ret_data;
	boost::algorithm::split(split_data, raw_data, boost::is_any_of("\n"));
	ret_data = _remove_blank_lines(split_data);
	raw_data = boost::algorithm::join(ret_data, "\n");
}	

std::vector<std::string> SPDataType::_remove_blank_lines(std::vector<std::string> input) {
	std::vector<std::string> ret;
	for (std::vector<std::string>::iterator line = input.begin();
			line != input.end(); line++) {
		if (boost::algorithm::trim_copy(*line).length() != 0)
			ret.push_back(*line);
	}
	return ret;
}

std::string SPDataType::_remove_whitespace(const std::string& line) {
	std::string temp = line;
	if (_value(line).find(":") != std::string::npos) { //Hackish fix here for : in value
		boost::algorithm::ireplace_all(temp, ":", ";");
		boost::algorithm::ireplace_first(temp, ";", ":");	
	}
	return boost::algorithm::trim_copy(temp);
}

/*
 * _string_convert: safe conversion of a YAML::Node
 * @param YAML::Node
 * @return pair<string, bool>
 *
 */
std::pair<std::string, bool> _string_convert(YAML::Node identifier) {
	try {
		return std::pair<std::string, bool>(identifier.as<std::string>(),true);
	} catch (YAML::TypedBadConversion<std::string> e) {
		std::string obj = std::string();
		return std::pair<std::string, bool>(obj,false);
	}
}

/*
 * _strip_string: converts num + string to num only
 * 	Assuming first 'word' is a number!
 * @param pair<string, bool>
 * @return pair<double, bool>
 *
 */
std::pair<double, bool> _strip_string(std::pair<std::string, bool> datum) {
	std::vector<std::string> splitted;
    boost::algorithm::split(splitted, datum.first, boost::is_any_of(" "));
	if (splitted.size() > 1 && boost::algorithm::iequals(splitted.at(1), "TB")) {
		return _strip_string(std::pair<std::string, bool>(splitted.at(0), datum.second), 1024);
	}
	std::pair<double, bool> ret;
	std::stringstream temp;
	try {
		temp << splitted.at(0);
		temp >> ret.first;
		ret.second = datum.second;
	} catch (std::invalid_argument e) {
		ret.first = 0.;
		ret.second = false;
		return ret;
	}
	return ret;
}

std::pair<double, bool> _strip_string(std::pair<std::string, bool> datum, unsigned multiplier) {
	std::pair<double, bool> temp = _strip_string(datum);
	return std::pair<double, bool>((temp.first * multiplier), temp.second);
}
