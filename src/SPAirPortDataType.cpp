#include "../libs/SPAirPortDataType.h"
#include <iostream>

SPAirPortDataType::SPAirPortDataType() : SPDataType() {
	card_type = str_bool;
	firmware = str_bool;
	supported_modes = str_bool;
	airdrop = bool_bool;
}


SPAirPortDataType::SPAirPortDataType(std::string raw_input) : SPDataType(raw_input) {
	YAML::Node temp = (_data.begin()->second)["Interfaces"];
	if (temp["en0"])
		temp = temp["en0"];
	else if (temp["en1"])
		temp = temp["en1"];
	else if (temp["en2"])
		temp = temp["en2"];
	else
		std::cout << "DIDNT FIND IT" << std::endl;

	card_type = _string_convert(temp["Card Type"]);
	firmware = _string_convert(temp["Firmware Version"]);
	supported_modes = _string_convert(temp["Supported PHY Modes"]);
	airdrop = _airdrop(temp);
}

std::pair<bool, bool> SPAirPortDataType::_airdrop(YAML::Node node) const {
	if (node["AirDrop"]) {
		if (boost::algorithm::iequals(_string_convert(node["AirDrop"]).first,
					"Supported")) {
			return std::pair<bool, bool>(true, true);
		}
		return std::pair<bool, bool>(false, true);
	}
	return std::pair<bool, bool>(false, false);
}

bool SPAirPortDataType::success() const {
	if (!firmware.second ||
			!card_type.second)
		return false;
	return true;
}

void SPAirPortDataType::set_all() {
	std::string temp_str;
	if (!supported_modes.second) {
		temp_str = _question("Does this have an AirPort card? [y/n] ");
		supported_modes.second = boost::algorithm::iequals(temp_str, "y");
	}
	if (!airdrop.second) {
		temp_str = _question("AirDrop? [y/n] ");
		set_airdrop(boost::algorithm::iequals(temp_str, "y"));
	}
}
