#include "../libs/SPBluetoothDataType.h"

std::pair<std::string, bool> SPBluetoothDataType::_get_address(YAML::Node node) const {
	YAML::Node temp;
	for (YAML::const_iterator it = node.begin(); it != node.end(); it++) {
		if (it->first.as<std::string>().find("Hardware") != std::string::npos) {
			temp = it->second;
			break;
		}
	}

	return _string_convert(temp["Address"]);
}	

void SPBluetoothDataType::set_all() {

	std::string temp_string;
		
	if (!this->address.second) {
		temp_string = _question("Is a bluetooth chip present in this computer? [y/n]");

		while(!boost::algorithm::iequals(temp_string,"y") && !boost::algorithm::iequals(temp_string,"n") ){
		 	std::cout << "Please enter  a 'y' for yes and a 'n' for no"<< std::endl;
		 	temp_string = _question("Is a bluetooth chip present in this computer? [y/n]");
		}
			
		set_address(temp_string);
		
	}
	
}

bool SPBluetoothDataType::success() const{
	return address.second;
}
