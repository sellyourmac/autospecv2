#include "../libs/SPMemoryDataType.h"

SPMemoryDataType::SPMemoryDataType() : SPDataType() {
	can_upgrade = bool_bool;
}

SPMemoryDataType::SPMemoryDataType(std::string raw_input) : SPDataType(raw_input) {
	//TODO Stuff
	YAML::Node node = _data;
	while(node.IsMap()) {
		if (!boost::algorithm::iequals(_string_convert(node["ECC"]).first, ""))
		       break;	
		node = node.begin()->second;
	}
	can_upgrade = _upgrade(_string_convert(node["Upgradeable Memory"]));
	for (YAML::const_iterator it = node["FUCKING A"].begin(); it != node["FUCKING A"].end(); it++) {
		_RAMSticks.push_back(it->second.as<RAMStick>());
		RAMStick* last_stick = &(*(_RAMSticks.end()-1));
		std::pair<std::pair<unsigned, bool>, std::pair<unsigned, bool> > 
			bankdimm = RAMStick::bank_dimm(it->first.as<std::string>());
		last_stick->BANK = bankdimm.first;
		last_stick->DIMM = bankdimm.second;
	}
}

std::pair<unsigned, bool> SPMemoryDataType::get_total_ram() const {
	std::pair<unsigned, bool> sum(0, true);
	for (unsigned long i = 0; i < _RAMSticks.size(); i++) {
		sum.first += _RAMSticks.at(i).size.first;
		sum.second = (sum.second && _RAMSticks.at(i).size.second);
	}
	return sum;
}

std::pair<unsigned, bool> SPMemoryDataType::get_ram_speed() const {
	bool ret = true;
	unsigned max = 0;
	std::map<unsigned, unsigned> stats;
	for (unsigned long i = 0; i < _RAMSticks.size(); i++) {
		std::pair<unsigned, bool> curr_speed = get_ram_speed(_RAMSticks.at(i));
		ret = ret && curr_speed.second;
		if (!stats.insert(std::pair<unsigned, unsigned>(curr_speed.first, 1)).second) {
			stats[curr_speed.first]++;
		}
		max = (curr_speed.first > max) ? curr_speed.first : max;
	}
	return std::pair<unsigned, bool>(max, ret);
}

std::pair<bool, bool> SPMemoryDataType::_upgrade(std::pair<std::string, bool> input) {
	if (!input.second)
		return std::pair<bool, bool>(false, false);
	if (boost::algorithm::iequals(input.first, "Yes"))
		return std::pair<bool, bool>(true, true);
	return std::pair<bool, bool>(false, true);
}

bool SPMemoryDataType::success() const {
	if (//!can_upgrade.second || !_upgrade.second ||
			_RAMSticks.size() == 0)
		return false;
	return true;
}

void SPMemoryDataType::set_all() {
	//can_upgrade is not implemented because it is not necessary for now
	//TODO Check with Tyler to see what he wants to do about it.
	if (_RAMSticks.empty()) {
		std::cout << "I'm sorry, but this shouldn't happen." <<
			std::endl << "No ram was detected, you will need to enter this in manually for now!" <<
			std::endl;
	}
}	

std::string SPMemoryDataType::output() const {
std::stringstream ss;
	
	ss<<"{\"MemorySlots\":{\"ECC\":{";
		for (std::vector<RAMStick>::const_iterator it = _RAMSticks.begin(); it != _RAMSticks.end(); it++) {
			ss<<"\"";
			ss<<"BANK"<<(it->BANK.first)<<"/DIMM"<<(it->DIMM.first)<<"\":{";
				ss<<"\"Status\":";
				ss<<"\"" << it->status.first << "\",";
				ss<<"\"Size\":"<<(it->size.first);
			ss<<"}";
			if(it != _RAMSticks.end()-1)
				ss<<",";
		}
		
	ss<<"}}}";
	return ss.str();



}


