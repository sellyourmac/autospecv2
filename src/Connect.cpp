#include "../libs/Connect.h"
#include <sstream>
#include <cstdlib>

namespace Connect {
void send(std::string data) {
	std::stringstream command;
	command << "python ";
	command << "./src/connect.py ";
	command << data;
	system(command.str().c_str());
}
}
