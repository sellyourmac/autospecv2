#include "../libs/SPPowerDataType.h"

SPPowerDataType::SPPowerDataType(std::string raw_input) : SPDataType(raw_input) {
	YAML::Node temp = _data["Power"];
	serial_number = _string_convert(temp["Battery Information"]["Model Information"]["Serial Number"]);
	charge_capacity = _charge_capacity(temp["Battery Information"]["Charge Information"]);
	cycle_count = _cycle_count(temp["Battery Information"]["Health Information"]);
	condition = _string_convert(temp["Battery Information"]["Health Information"]["Condition"]);
}

std::pair<unsigned, bool> SPPowerDataType::_charge_capacity(const YAML::Node& node) {
	for (YAML::const_iterator it = node.begin(); it != node.end(); it++) {
		if (boost::algorithm::iequals(it->first.as<std::string>(), "full charge capacity (mah)"))
			return std::pair<unsigned, bool>(it->second.as<unsigned>(), true);
	}
	return std::pair<unsigned, bool>(0,false);
}

std::pair<unsigned, bool> SPPowerDataType::_cycle_count(const YAML::Node& node) {
	for (YAML::const_iterator it = node.begin(); it != node.end(); it++) {
		if (boost::algorithm::iequals(it->first.as<std::string>(), "cycle count"))
			return std::pair<unsigned, bool>(it->second.as<unsigned>(), true);
	}
	return std::pair<unsigned, bool>(0, false);
}

bool SPPowerDataType::success() const {

	if (!charge_capacity.second|| !cycle_count.second || !condition.second)

		return false;

	else 

		return true;

}

std::string SPPowerDataType::output() const {
	std::stringstream ss;
	ss<<"{";
		ss<<"\"BatteryInformation\":{";
			ss<<"\"ChargeInformation\":{";
				ss<<"\"FullchargeCapacity(mAh)\":"<<charge_capacity.first; 
			ss<<"},";
			ss<<"\"HealthInformation\":{";
				ss<<"\"CycleCount\":"<< cycle_count.first; ss<<",";
				ss<<"\"Condition\":"<< "\""<<condition.first; ss<<"\"";
			ss<<"}";
		ss<<"}";


	ss<<"}";
	return ss.str();
}


