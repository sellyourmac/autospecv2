#include "../libs/SPDisplaysDataType.h"
#include <iostream>
#include <fstream>

SPDisplaysDataType::SPDisplaysDataType() : SPDataType() {
}

SPDisplaysDataType::SPDisplaysDataType(std::string raw_input) : SPDataType(raw_input) {
	YAML::Node node;
	Retina.first = false;
	Retina.second = false;
	node = _data.begin()->second;
	for (YAML::const_iterator it = node.begin(); it != node.end(); it++) {
		_GPUList.push_back((it->second).as<GPU>());
		if (_GPUList.size() != 0) {
			Retina.first = Retina.first || _GPUList.at(_GPUList.size()-1).Retina.first;
			Retina.second = true;

			if (_GPUList.at(_GPUList.size()-1).VRAM.first == 32768) {
				_GPUList.at(_GPUList.size()-1).VRAM.first = 256;
			}
		}
	}
	if (Retina.first && Retina.second) {
		retina_manufacturer.first = _get_retina_manufacturer();
		retina_manufacturer.second = true;
	} else {
		retina_manufacturer.first = "N/A";
		retina_manufacturer.second = true;
	}
}

std::pair<std::string, bool> SPDisplaysDataType::get_chipset_model(GPU gpu) const {
	if (!gpu.ChipsetModel.second)
		return std::pair<std::string, bool>("", false);
	return gpu.ChipsetModel;
}

std::pair<unsigned, bool> SPDisplaysDataType::get_vram(GPU gpu) const {
	if (!gpu.VRAM.second)
		return std::pair<unsigned, bool>(0, false);
	return gpu.VRAM;
}

std::pair<std::string, bool> SPDisplaysDataType::get_vendor(GPU gpu) const {
	if (!gpu.Vendor.second)
		return std::pair<std::string, bool>("", false);
	return gpu.Vendor;
}

std::pair<bool, bool> SPDisplaysDataType::get_retina() const {
	if (!Retina.second)
		return std::pair<bool, bool>(false, false);
	return Retina;
}

bool SPDisplaysDataType::success() const {
	if (_GPUList.empty())
		return false;
	else
		return true;
}

void SPDisplaysDataType::set_all() {
	if (_GPUList.empty()) {
		add_gpu();
		for (std::vector<GPU>::const_iterator it = _GPUList.begin(); it != _GPUList.end(); it++) {
			if (_GPUList.size() != 0) {
				Retina.first = Retina.first || _GPUList.at(_GPUList.size()-1).Retina.first;
				Retina.second = true;

				if (_GPUList.at(_GPUList.size()-1).VRAM.first == 32768) {
					_GPUList.at(_GPUList.size()-1).VRAM.first = 256;
				}
			}
		}
		if (Retina.first && Retina.second) {
			retina_manufacturer.first = _get_retina_manufacturer();
			retina_manufacturer.second = true;
		} else {
			retina_manufacturer.first = "N/A";
			retina_manufacturer.second = true;
		}	
	}
}

void SPDisplaysDataType::add_gpu() {
	std::string model;
	bool retina;
	unsigned vram;
	GPU gpu;

	std::stringstream temp_str;
	model = _question("GPU Model");
	temp_str << _question("Retina? [y/n]");
	if (boost::algorithm::iequals(temp_str.str(), "y"))
		retina = true;
	else 
		retina = false;
	temp_str.str("");
	temp_str << _question("VRAM in MB?");
	
	temp_str >> vram;
	
	gpu.ChipsetModel = std::pair<std::string, bool>(model, true);
	gpu.Retina = std::pair<bool, bool>(retina, true);
	gpu.VRAM = std::pair<unsigned, bool>(vram, true);
	this->_GPUList.push_back(gpu);
}


std::string SPDisplaysDataType::output() const {
	std::stringstream ss;
	ss<<"{";
		for (std::vector<GPU>::const_iterator it = _GPUList.begin(); it != _GPUList.end(); it++) {
			ss<<"\"";
			ss<<(it->ChipsetModel.first)<<"\":{";
			ss<<"\"ChipsetModel\": \""<<(it->ChipsetModel.first)<<"\",";
			ss<<"\"VRAM(Total)\": \""<<(it->VRAM.first)<<"\"";
			ss<<"},";
		}
		//TODO: Actually implement
		
		ss<<"\"Retina\":";
		if(Retina.first)
			ss<<"true,";
		else
			ss<<"false,";
		ss<<"\"RetinaManufacturer\":"<<"\""<<retina_manufacturer.first; ss<<"\"";



	ss<<"}";
	return ss.str();
}


std::string SPDisplaysDataType::_get_retina_manufacturer() {
	std::stringstream command;
	command << "ioreg -lw0 | grep \"EDID\" | sed \"/[^<]*</s///\" | xxd -p -r | strings -6";
	std::string filename = "/tmp/manufacturer";
	command << " > " << filename;
	system(command.str().c_str());

	std::ifstream input_file(filename.c_str(), std::ifstream::in);
	std::string raw_data;
	if (input_file) {
		std::stringstream buffer;
		buffer << input_file.rdbuf();
		input_file.close();
		raw_data = buffer.str();
	} else {
		return "";
	}
	std::string ret;
	if (raw_data.find("LP") != std::string::npos)
		ret = "LG";
	else if (raw_data.find("LSN") != std::string::npos)
		ret = "Samsung";
	else
		ret = "NOT FOUND";
	return ret;
}
