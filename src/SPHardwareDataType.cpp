#include "../libs/SPHardwareDataType.h"
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <vector>

SPHardwareDataType::SPHardwareDataType() : SPDataType() {
	ModelName = str_bool;
	ModelIdentifier = str_bool;
	ProcessorName = str_bool;
	ProcessorSpeed = u_bool;
	NumProcessors = u_bool;
	TotalNumCores = u_bool;
	Memory = u_bool;
	SerialNumber = str_bool;
}

SPHardwareDataType::SPHardwareDataType(std::string raw_data) : SPDataType(raw_data) {
	_data = _data["Hardware"]["Hardware Overview"];

	ModelName = _string_convert(_data["Model Name"]);
	ModelIdentifier = _string_convert(_data["Model Identifier"]);
	ProcessorName = _string_convert(_data["Processor Name"]);
	ProcessorSpeed = _strip_string(_string_convert(_data["Processor Speed"]));
	NumProcessors = _strip_string(_string_convert(_data["Number of Processors"]));
	if (NumProcessors.second == false) {
		NumProcessors = _strip_string(_string_convert(_data["Number Of Processors"]));
	}
	TotalNumCores = _strip_string(_string_convert(_data["Total Number of Cores"]));
	if (TotalNumCores.second == false) {
		TotalNumCores = _strip_string(_string_convert(_data["Total Number Of Cores"]));
	}
	Memory = _strip_string(_string_convert(_data["Memory"]), MEMORY_MULTIPLIER);
	SerialNumber = _string_convert(_data["Serial Number (system)"]);
}

bool SPHardwareDataType::is_SATA() const {
	if (this->get_modelidentifier().second) {
		if (boost::algorithm::iequals(this->get_modelidentifier().first,
					"MacBookAir1,1"))
			return false;
	}
	return true;
}

bool SPHardwareDataType::has_battery() const {
	if (this->get_modelidentifier().first.at(0) == 'i') //cheezy iMac test
		return false;
	else if (this->get_modelidentifier().first.at(3) == 'P') //cheezy MacPro test
		return false;
	else if (this->get_modelidentifier().first.at(0) == 'M' && this->get_modelidentifier().first.at(3) == 'm')
		return false;
	return true;
}

bool SPHardwareDataType::has_diskdrive() const {
	if (this->get_modelidentifier().first.length() >= 8 && this->get_modelidentifier().first.at(7) == 'A') // MacBookAir test
		return false;
	else if (this->get_modelidentifier().first.length() >= 12 && this->get_modelidentifier().first.at(11) != ',') // MacBookPro > 10,1
		return false;
	else if (boost::algorithm::iequals("macbook8,1",this->get_modelidentifier().first))
		return false;
	else if (this->get_modelidentifier().first.at(0) == 'i' && this->get_modelidentifier().first.at(5) != ',' &&
			this->get_modelidentifier().first.at(5) != '0' && this->get_modelidentifier().first.at(5) != '1' &&
			this->get_modelidentifier().first.at(5) != '2')
		return false;
	return true;
}

bool SPHardwareDataType::success() const {
	if (!get_modelname().second || !get_modelidentifier().second ||
			!get_processorname().second || !get_processorspeed().second ||
			!get_numprocessors().second || !get_totalnumcores().second ||
			!get_memory().second)
		return false;
	return true;
}

void SPHardwareDataType::set_all() {
	std::stringstream temp_str;
	if (!this->ModelName.second) {
		set_modelname(_question("Model Name", this->ModelName.first));
	}
	if (!this->ModelIdentifier.second) {
		set_modelidentifier(_question("Model Identifier", this->ModelIdentifier.first));
	}
	if (!this->ProcessorName.second) {
		set_processorname(_question("Processor Name", this->ProcessorName.first));
	}
	if (!this->ProcessorSpeed.second) {
		try {
			temp_str << _question("Processor Speed (GHz)", this->ProcessorSpeed.first);
			temp_str >> this->ProcessorSpeed.first;
			this->ProcessorSpeed.second = true;
		} catch (std::exception) {
			;
		}
		temp_str.rdbuf();
		temp_str.clear();
	}
	if(!this->NumProcessors.second) {
		try {
			temp_str << _question("Number of Processors", this->NumProcessors.first);
			temp_str >> this->NumProcessors.first;
			this->NumProcessors.second = true;
		} catch (std::exception) {
			;
		}
		temp_str.rdbuf();
		temp_str.clear();
	}
	if (!this->TotalNumCores.second) {
		try {
			temp_str << _question("Total Number of Cores", this->TotalNumCores.first);
			temp_str >> this->TotalNumCores.first;
			this->TotalNumCores.second = true;
		} catch (std::exception) {
			;
		}
		temp_str.rdbuf();
		temp_str.clear();
	}
	if (!this->Memory.second) {
		try {
			temp_str << _question("Memory in MB", this->Memory.first);
			temp_str >> this->Memory.first;
			this->Memory.second = true;
		} catch (std::exception) {
			;
		}
		temp_str.rdbuf();
		temp_str.clear();
	}
	if (!this->SerialNumber.second) {
		set_serialnumber(_question("Serial Number", this->SerialNumber.first));
	}

}
std::string SPHardwareDataType::output() const{
	std::stringstream ss;
	ss<<"{";
		ss<<"\"HardwareOverview\":{";
			ss<<"\"ProcessorName\":"; ss<<"\""<<ProcessorName.first; ss<<"\",";
			ss<<"\"ModelIdentifier\":";ss<<"\""<<ModelIdentifier.first; ss<<"\",";
			ss<<"\"ModelName\":";ss<<"\""<<ModelName.first; ss<<"\",";
			ss<<"\"SerialNumber(system)\":";ss<<"\""<<SerialNumber.first; ss<<"\",";
			ss<<"\"TotalNumberOfCores\":";ss<<TotalNumCores.first; ss<<",";
			ss<<"\"NumberOfProcessors\":";ss<<NumProcessors.first; ss<<",";
			ss<<"\"Memory\":";ss<<Memory.first; ss<<",";
			ss<<"\"ProcessorSpeed\":";ss<<"\""<<ProcessorSpeed.first; ss<<"\"";
		ss<<"}";
	
	ss<<"}";
	return ss.str();
}


