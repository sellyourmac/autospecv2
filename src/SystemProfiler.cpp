#include "../libs/SystemProfiler.h"

#include <fstream>
#include <sstream>
#include <cstdlib>

namespace SystemProfiler {

	std::string get_data(std::string key) {
#if defined(__APPLE__) || defined(__MACH__)
		std::string command = "system_profiler " + key;
		command += " > /tmp/"+key;
		system(command.c_str());
		std::string ifile = "/tmp/" + key;
		std::ifstream input_file(ifile.c_str(), std::ifstream::in);
		std::string raw_data;
		if (input_file) {
			std::stringstream buffer;
			buffer << input_file.rdbuf();
			input_file.close();
			raw_data = buffer.str();
		} else {
			return "";
		}
		return raw_data;
#else
		std::string input_file = "docs/MacBookPro10,1-"+key+".txt";
		std::ifstream ifile(input_file.c_str(), std::ifstream::in);
		std::string ret = "";
		if (ifile) {
			std::stringstream buffer;
			buffer << ifile.rdbuf();
			ifile.close();
			ret = buffer.str();	
		}
		return ret;
#endif
	}	
}
