#include "../libs/Config.h"

bool Config::is_ATA(SPHardwareDataType& hdware) {
	std::string modelidentifier = hdware.get_modelidentifier().first;
	for (auto ident : ata_list) {
		if (boost::algorithm::iequals(ident, modelidentifier))
			return true;
	}
	return false;
}
