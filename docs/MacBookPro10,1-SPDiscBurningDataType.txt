Disc Burning:

    HL-DT-ST DVDRW  GS21N:

      Firmware Revision: SA18
      Interconnect: ATAPI
      Burn Support: Yes (Apple Shipping Drive)
      Cache: 2048 KB
      Reads DVD: Yes
      CD-Write: -R, -RW
      DVD-Write: -R, -R DL, -RW, +R, +R DL, +RW
      Write Strategies: CD-TAO, CD-SAO, CD-Raw, DVD-DAO
      Media: To show the available burn speeds, insert a disc and choose View > Refresh
	  