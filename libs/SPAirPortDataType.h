#ifndef AIRPORT_H
#define AIRPORT_H

#include "DataTypes.h"

class SPAirPortDataType : public SPDataType {
	public:
		SPAirPortDataType();
		SPAirPortDataType(std::string raw_input);
		std::pair<std::string, bool> get_card_type() const {return card_type;}
		std::pair<std::string, bool> get_firmware() const {return firmware;}
		std::pair<std::string, bool> get_supported_modes() const {return supported_modes;}
		std::pair<bool, bool> get_airdrop() const {return airdrop;}
		bool success() const;
		void set_all();
		void set_airdrop(bool input) {
			airdrop.first = input;
			airdrop.second = true;
		}
	private:
		std::pair<std::string, bool> card_type;
		std::pair<std::string, bool> firmware;
		std::pair<std::string, bool> supported_modes; //Check THIS ONE for existence of card
		std::pair<bool, bool> airdrop;

		std::pair<bool, bool> _airdrop(YAML::Node node) const;
};

#endif
