#ifndef SYSTEMPROFILER_H
#define SYSTEMPROFILER_H

#include <string>
#include <vector>

namespace SystemProfiler {
	std::string get_data(std::string key);
}

#endif
