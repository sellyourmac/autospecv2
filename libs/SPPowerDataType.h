#ifndef POWERDATATYPE_H
#define POWERDATATYPE_H

#include "DataTypes.h"

class SPPowerDataType : public SPDataType {
	public:
		SPPowerDataType() : SPDataType() {
			serial_number = str_bool;
			charge_capacity = u_bool;
			cycle_count = u_bool;
			condition = str_bool;
		}
		SPPowerDataType(std::string raw_input);
		std::pair<std::string, bool> get_serial_number() const {return serial_number;}
		std::pair<unsigned, bool> get_charge_capacity() const {return charge_capacity;}
		std::pair<unsigned, bool> get_cycle_count() const {return cycle_count;}
		std::pair<std::string, bool> get_condition() const {return condition;}
		bool success() const;
		void set_all() {

			std::cout << "Battery information requires manual user input on the inventory page." << std::endl;

		}
		std::string output() const;
		
	private:
		std::pair<std::string, bool> serial_number;
		std::pair<unsigned, bool> charge_capacity;
		std::pair<unsigned, bool> cycle_count;
		std::pair<std::string, bool> condition;
		
		std::pair<unsigned, bool> _charge_capacity(const YAML::Node& node);	
		std::pair<unsigned, bool> _cycle_count(const YAML::Node& node);
};

#endif
