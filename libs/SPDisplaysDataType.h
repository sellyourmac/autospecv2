#ifndef DISPLAYS_H
#define DISPLAYS_H

#include "DataTypes.h"
#include <yaml-cpp/yaml.h>
#include <vector>
#include <iostream>

class SPDisplaysDataType : public SPDataType {
	public:
		SPDisplaysDataType();
		SPDisplaysDataType(std::string raw_input);
		std::pair<std::string, bool> get_chipset_model(unsigned displayNum) const {
			if (displayNum > _GPUList.size()-1)
				throw(std::out_of_range("GPU does not exist!"));
			return get_chipset_model(_GPUList.at(displayNum));
		}
		std::pair<std::string, bool> get_chipset_model(GPU gpu) const;
		std::pair<unsigned, bool> get_vram(unsigned displayNum) const {
			if (displayNum > _GPUList.size()-1)
				throw(std::out_of_range("GPU does not exist!"));
			return get_vram(_GPUList.at(displayNum));
		}
		std::pair<unsigned, bool> get_vram(GPU gpu) const;
		std::pair<std::string, bool> get_vendor(unsigned displayNum) const {
			if (displayNum > _GPUList.size()-1)
				throw(std::out_of_range("GPU does not exist!"));
			return get_vendor(_GPUList.at(displayNum));
		}
		std::pair<std::string, bool> get_vendor(GPU gpu) const;
		std::pair<bool, bool> get_retina() const;
		bool success() const;
		void set_all();
		void set_vram(unsigned input) {
			this->_GPUList.at(0).VRAM.first = input;
			this->_GPUList.at(0).VRAM.second = true;
		}
		void set_retina(bool input) {
			this->_GPUList.at(0).Retina.first = input;
			this->_GPUList.at(0).Retina.second = true;
		}
		void add_gpu();
		std::string output() const;
	private:
		std::vector<GPU> _GPUList;
		std::pair<bool, bool> Retina;
		std::pair<std::string, bool> retina_manufacturer;

		std::string _get_retina_manufacturer();
};

namespace YAML {
template<>
struct convert<GPU> {
	static Node encode(const GPU& rhs) {
		Node node;
		node.push_back(rhs.ChipsetModel.first);
		node.push_back(rhs.VRAM.first);
		node.push_back(rhs.Vendor.first);
		node.push_back(rhs.Retina.first);
		return node;
	}

	static bool decode(const Node& node, GPU& rhs) {
		if (!node.IsMap()) {
			return false;
		}
		rhs.ChipsetModel = _string_convert(node["Chipset Model"]);
		rhs.VRAM = std::pair<unsigned, bool>(0, false);
		for (const_iterator tempNode = node.begin(); tempNode != node.end(); tempNode++) {
			std::pair<std::string, bool> tempKey = _string_convert(tempNode->first);
			//std::cout << tempKey.first << " " << tempKey.second << std::endl;
			if (tempKey.second) {
				if (tempKey.first.find("VRAM") != std::string::npos) {
					rhs.VRAM = _strip_string(_string_convert(tempNode->second));
				}
			}
		}
		rhs.Vendor = _string_convert(node["Vendor"]);
		rhs.Retina = std::pair<bool, bool>(false, false);
		for (const_iterator tempNode = node["Displays"].begin(); 
				tempNode != node["Displays"].end(); tempNode++) {
			if (tempNode->second["Retina"] && boost::algorithm::iequals(_string_convert(tempNode->second["Retina"]).first, "Yes"))
				rhs.Retina.first = true;
		}
		rhs.Retina.second = true;
		return true;
	}
};
}


#endif
