#ifndef SPDATA_H
#define SPDATA_H
#include "yaml-cpp/yaml.h"
//#include "DataStructures.h"
#include <vector>
#include <string>
#include <iostream>
#include <boost/algorithm/string.hpp>

class SPDataType {
	public:
		SPDataType() {SPDataType("SPNOTHING");}
		SPDataType(std::string raw_data, std::string type = "");
		std::string get_data() const {return _clean_data;}
		const YAML::Node& get_yaml() const {return _data;}
		virtual bool success() const {return _data.size() != 0;}
		virtual void set_all();
	private:
		std::string _type;
		std::vector<std::string> _keys;
		std::string _clean_data;
	protected:
		YAML::Node _data;

		std::pair<std::string, bool> str_bool; 
		std::pair<unsigned, bool> u_bool;
		std::pair<bool, bool> bool_bool;

		std::string _sanitize(std::string& raw_data);
		static std::string _value(const std::string line);
		static std::string _key(const std::string line);
		static int _whitespace_count(const std::string line);	
		static void _remove_blank_lines(std::string& raw_data);
		static std::vector<std::string> _remove_blank_lines(std::vector<std::string> input);
		static std::string _remove_whitespace(const std::string& line);	
		virtual std::string _question(std::string name);
		template<typename T> std::string _question(std::string name, T& current);
};

std::pair<std::string, bool> _string_convert(YAML::Node identifier);
std::pair<double, bool> _strip_string(std::pair<std::string, bool> datum);
std::pair<double, bool> _strip_string(std::pair<std::string, bool> datum, unsigned multiplier);

template<typename T>
std::string SPDataType::_question(std::string name, T& current) {
	std::string ret;
	std::stringstream temp;
	std::cout << "The current value for '" << name << "' is '" <<
		current << "'. Press ENTER to keep value or enter new value:" << std::endl;
	std::getline(std::cin, ret);
	if (ret.length() == 0) {
		temp << current;
		return temp.str();
	}
	std::cout << "Is '" << ret << "' correct? [y/n]: " << std::endl;
	std::string confirm;
	std::getline(std::cin, confirm);
	if (boost::algorithm::iequals(confirm, "n"))
		return _question(name, current);
	return ret;
}


#endif
