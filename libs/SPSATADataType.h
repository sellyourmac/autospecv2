#ifndef SPSATA_H
#define SPSATA_H

#define SATA 1
#define ATA 0

#define SSD 1
#define ROTATIONAL 0

#include "DataTypes.h"
#include <yaml-cpp/yaml.h>
#include <boost/fusion/container/vector.hpp>

const unsigned SSD_SIZE_LIST[7] = {64,128,240,256,512,768,1024};
const unsigned MECH_SIZE_LIST[13] = {60,80,100,120,160,200,250,320,500,750,1024,1536,2048};

class SPSATADataType : public SPDataType {
	public:
		SPSATADataType();
		SPSATADataType(std::string raw_data);

		
		std::pair<bool, bool> is_SSD() const {return this->is_SSD(0);} //defaults to 0
		std::pair<bool, bool> is_SSD(unsigned drive_num) const;
		inline std::pair<bool, bool> is_SSD(const HardDrive& drive) const;
		unsigned get_capacity() const {return get_capacity(0);} //defaults to 0
		unsigned get_capacity(std::size_t drive_num) const; //starting with 0
		unsigned get_capacity(const HardDrive& drive) const;
		unsigned get_total_capacity() const;
		std::pair<bool, bool> get_connection_type() const {return connectionType;};
		std::pair<std::string, bool> get_vendor() const {return vendor;}
		std::pair<std::string, bool> get_product() const {return product;}
		bool success() const;
		void set_all();
		void set_vendor(std::string input) {
			vendor.first = input;
			vendor.second = true;
		}
		void set_product(std::string input) {
			product.first = input;
			product.second = true;
		}
		void set_connectiontype(bool input) {
			connectionType.first = input;
			connectionType.second = true;
		}
		void add_harddrive();
		std::string output() const;
	private:
		std::pair<std::string, bool> vendor;
		std::pair<std::string, bool> product;
		std::pair<bool, bool> connectionType; // true = sata, false = ata
		std::vector<HardDrive> _HDDList;

		std::pair<bool, bool> _connection_type(std::pair<std::string, bool> input);
};

namespace YAML {
template<>
struct convert<HardDrive> {
	static Node encode(const HardDrive& rhs) {
		Node node;
		node.push_back(rhs.Capacity);
		node.push_back(rhs.Model);
		node.push_back(rhs.SerialNumber);
		node.push_back(rhs.MediumType);
		node.push_back(rhs.TRIMSupport);
		node.push_back(rhs.SMARTStatus);
		return node;
	}

	static bool decode(const Node& node, HardDrive& rhs) {
		if (!node.IsMap()) {
			return false;
		}
		rhs.Capacity = _strip_string(_string_convert(node["Capacity"]));
		rhs.Model = _string_convert(node["Model"]);
		rhs.SerialNumber = _string_convert(node["Serial Number"]);
        if (rhs.Model.first.find("SSD") != std::string::npos) {
            rhs.MediumType = std::pair<bool,bool>(true,true);
        } else {
            rhs.MediumType = HardDrive::medium_type(_string_convert(node["Medium Type"])); //TODO sanity check
        }
		rhs.TRIMSupport = HardDrive::trim(_string_convert(node["TRIM Support"])); 
		rhs.SMARTStatus = _string_convert(node["S.M.A.R.T. status"]); //TODO standardize
		rhs.Rate = _strip_string(_string_convert(node["Rotational Rate"]));
		return true;
	}
};
}

#endif
