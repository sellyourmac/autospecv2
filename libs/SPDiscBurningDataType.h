#ifndef DISCBURNING_H
#define DISCBURNING_H

#include "DataTypes.h"

class SPDiscBurningDataType : public SPDataType {
	public:
		SPDiscBurningDataType() : SPDataType() {dvd_read = bool_bool; cd_write = str_bool;};
		SPDiscBurningDataType(std::string raw_input);
		std::pair<bool, bool> get_dvd_read() const {return dvd_read;}
		std::pair<std::string, bool> get_cd_write() const {return cd_write;}
		bool is_empty() const {return !(dvd_read.second || cd_write.second);}
		bool success() const;
		void set_all() {
			std::cout << "Disc Drive information requires manual user input on the inventory page." << std::endl;
		}
		std::string output() const;
	private:
		std::pair<bool, bool> dvd_read;
		std::pair<std::string, bool> dvd_write;
		std::pair<std::string, bool> cd_write;
		std::pair<std::string, bool> model;

		std::pair<bool, bool> _get_dvd_read(YAML::Node node) const;
		std::pair<std::string, bool> _get_cd_write(YAML::Node node) const;
};

#endif
