#ifndef DISPLAY_H
#define DISPLAY_H

#include "main.h"

class Display {
	public:
		Display();
		void run();
		void fix();
		//void submit();
		std::string  _get_JSon();
		std::string  _get_ID();
		std::string  _get_Username();
	private:
		std::vector<SPDataType*> to_fix;
		SPHardwareDataType hardware;
		SPSATADataType sata;
		SPAirPortDataType airport;
		SPBluetoothDataType bluetooth;
		SPDiscBurningDataType disc;
		SPDisplaysDataType display;
		SPMemoryDataType memory;
		SPPowerDataType power;
		std::string _get_data(std::string type_name);
		bool _check_success(std::string input, SPDataType& curr_obj);
		
};

#endif
