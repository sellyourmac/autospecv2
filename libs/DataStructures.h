#ifndef DATASTRUCTURES_H
#define DATASTRUCTURES_H

#include <yaml-cpp/yaml.h>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <boost/lexical_cast.hpp>

struct HardDrive {
	std::pair<unsigned, bool> Capacity;
	std::pair<std::string, bool> Model;
	std::pair<std::string, bool> SerialNumber;
	std::pair<bool, bool> MediumType; //true == SSD, false = mechanical
	std::pair<bool, bool> TRIMSupport;
	std::pair<std::string, bool> SMARTStatus;
	std::pair<unsigned, bool> Rate;

	static std::pair<bool, bool> trim(std::pair<std::string, bool> input) {
		if (!input.second)
			return std::pair<bool, bool>(false, false);
		if (boost::algorithm::iequals(input.first, "yes"))
			return std::pair<bool, bool>(true, true);
		else if (boost::algorithm::iequals(input.first, "no"))
			return std::pair<bool, bool>(false, true);
		return std::pair<bool, bool>(false, false);
	}

	static std::pair<bool, bool> medium_type(std::pair<std::string, bool> input) {
		if (!input.second)
			return std::pair<bool, bool>(false, true);
		if (boost::algorithm::iequals(input.first, "Solid State"))
			return std::pair<bool, bool>(true, true);
		return std::pair<bool, bool>(false, true); //Rotational
	}
};

struct GPU {
	std::pair<std::string, bool> ChipsetModel;
	std::pair<unsigned, bool> VRAM;
	std::pair<std::string, bool> Vendor;
	std::pair<bool, bool> Retina;
};

struct RAMStick {
	std::pair<unsigned, bool> BANK;
	std::pair<unsigned, bool> DIMM;
	std::pair<unsigned, bool> size;
	std::pair<unsigned, bool> speed;
	std::pair<std::string, bool> status;

	static std::pair<std::pair<unsigned, bool>, std::pair<unsigned, bool> > bank_dimm(std::string stick) {
		std::string::size_type split = stick.find('/');
		std::pair<unsigned, bool> p_bank, p_dimm;
		unsigned bank, dimm;
		try {
			std::stringstream ss;
			std::string s;
			char c = stick.at(split-1);
			ss << c;
			ss >> s;
			
			if( boost::algorithm::iequals(s,"a")){
				bank = 0;
				p_bank.second = true;
			}
			else if(boost::algorithm::iequals(s,"b")){
				bank = 1;
				p_bank.second = true;
			}
			else{
				bank = boost::lexical_cast<unsigned>(stick.at(split-1));
				p_bank.second = true;
			}
		} catch (std::exception) {
			bank = 0;
			p_bank.second = false;
		}
		p_bank.first = bank;
		
		try {
			dimm = boost::lexical_cast<unsigned>(stick.at(stick.size()-1));
			p_dimm.second = true;
		} catch (std::exception) {
			dimm = 0;
			p_dimm.second = false;
		}
		p_dimm.first = dimm;
		
		return std::pair<std::pair<unsigned, bool>, std::pair<unsigned, bool> >(p_bank, p_dimm);
	}
};

#endif
