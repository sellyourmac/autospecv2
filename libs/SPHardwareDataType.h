
#ifndef SPHARDWARE_H
#define SPHARDWARE_H

#define MEMORY_MULTIPLIER 1024

#include "DataTypes.h"

class SPHardwareDataType : public SPDataType {
	public:
		SPHardwareDataType();
		SPHardwareDataType(std::string raw_data);
		const YAML::Node& get_yaml() const {return _data;}
		std::pair<std::string, bool> get_modelname() const {return ModelName;}
		std::pair<std::string, bool> get_modelidentifier() const {return ModelIdentifier;}
		std::pair<std::string, bool> get_processorname() const {return ProcessorName;}
		std::pair<double, bool> get_processorspeed() const {return ProcessorSpeed;}
		std::pair<unsigned, bool> get_numprocessors() const {return NumProcessors;}
		std::pair<unsigned, bool> get_totalnumcores() const {return TotalNumCores;}
		std::pair<unsigned, bool> get_memory() const {return Memory;}
		std::pair<std::string, bool> get_serialnumber() const {return SerialNumber;}
		bool is_SATA() const;
		bool has_battery() const;
		bool has_diskdrive() const;
		bool success() const;
		void set_all();
		void set_modelname(std::string input) {
			this->ModelName.first = input;
			this->ModelName.second = true;
		}
		void set_modelidentifier(std::string input) {
			this->ModelIdentifier.first = input;
			this->ModelIdentifier.second = true;
		}	
		void set_processorname(std::string input) {
			this->ProcessorName.first = input;
			this->ProcessorName.second = true;
		}
		void set_processorspeed(double input) {
			this->ProcessorSpeed.first = input;
			this->ProcessorSpeed.second = true;
		}
		void set_numprocessors(unsigned input) {
			this->NumProcessors.first = input;
			this->NumProcessors.second = true;
		}
		void set_totalnumcores(unsigned input) {
			this->TotalNumCores.first = input;
			this->TotalNumCores.second = true;
		}
		void set_memory(unsigned input) {
			this->Memory.first = input;
			this->Memory.second = true;
		}
		void set_serialnumber(std::string input) {
			this->SerialNumber.first = input;
			this->SerialNumber.second = true;
		}
		std::string output() const;
		
	private:
		// bool = true when conversion is successful
		// false means user input is req'd
		std::pair<std::string, bool> ModelName;
		std::pair<std::string, bool> ModelIdentifier;
		std::pair<std::string, bool> ProcessorName;
		std::pair<double, bool> ProcessorSpeed; //GHz
		std::pair<unsigned, bool> NumProcessors;
		std::pair<unsigned, bool> TotalNumCores;
		std::pair<unsigned, bool> Memory; //MB
		std::pair<std::string, bool> SerialNumber;

};

#endif
