#ifndef BLUETOOTH_H
#define BLUETOOTH_H

#include "DataTypes.h"

class SPBluetoothDataType : public SPDataType {
	public:
		SPBluetoothDataType() : SPDataType() {address = str_bool;}
		SPBluetoothDataType(std::string raw_input) : SPDataType(raw_input) {
			address = _get_address(_data["Bluetooth"]);
		}
		bool success() const;
		void set_all();
		void set_address(std::string input) {
			this->address.first = input;
			this->address.second = true;
		}	

		std::pair<std::string, bool> get_address() const {return address;}
	private:
		std::pair<std::string, bool> address;
		std::pair<std::string, bool> _get_address(YAML::Node node) const;
};

#endif
