
#ifndef MEMORYDATATYPE_H
#define MEMORYDATATYPE_H

#define MEMORY_MULTIPLIER 1024

#include "DataTypes.h"
#include <vector>
#include <iostream>

class SPMemoryDataType : public SPDataType {
	public:
		SPMemoryDataType();
		SPMemoryDataType(std::string raw_input);

		std::pair<unsigned, bool> get_total_ram() const;
		std::vector<unsigned> get_ram_layout() const; //Umm. This doesn't follow style
		std::pair<bool, bool> upgradable() const {return can_upgrade;}
		std::pair<unsigned, bool> get_ram_speed() const;
		std::pair<unsigned, bool> get_ram_speed(RAMStick stick) const {return stick.speed;}
		bool success() const;
		void set_all();
		void set_upgrade(bool input) {
			can_upgrade.first = input;
			can_upgrade.second = true;
		}
		std::string output() const;
	private:
		std::pair<bool, bool> can_upgrade;
		std::vector<RAMStick> _RAMSticks;
		std::pair<bool, bool> _upgrade(std::pair<std::string, bool> input);
};

namespace YAML {
template<>
struct convert<RAMStick> {
	static Node encode(const RAMStick& rhs) {
		Node node;
		return node;
	}

	static bool decode(const Node& node, RAMStick& stick) {
		if (!node.IsMap()) 
			return false;
		stick.size = _strip_string(_string_convert(node["Size"]), MEMORY_MULTIPLIER);
		stick.speed = _strip_string(_string_convert(node["Speed"]));
		stick.status = _string_convert(node["Status"]);
		return true;
	}
};
}

#endif
